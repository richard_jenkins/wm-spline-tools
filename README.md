# SplineTools

Various tools to aid users and administrators of Wikimedia projects.

## Whois

A WHOIS service capable of performing IP and hostname lookups.

## License

See [here](https://bitbucket.org/richard_jenkins/wm-spline-tools/wiki/License).